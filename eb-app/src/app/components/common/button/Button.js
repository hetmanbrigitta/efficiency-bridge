import React from 'react';

const Button = ({ label }) => {
	return (
		<button type="submit" className="main-btn active-btn">{ label }</button>
	);
}

export default Button;
