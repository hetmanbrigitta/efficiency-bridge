import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { useAuth } from '../../../../context/AuthContext';

import Button from '../button/Button';

const AuthForm = ({ label, registerView}) => {
	const navigate = useNavigate();
	const { login, register } = useAuth();
	const [error, setError] = useState('');

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [passwordConfirmation, setPasswordConfirmation] = useState('');

	const handleAction = async() => {
		if (registerView && (password !== passwordConfirmation)) {
			return setError('Passwords do not match!');
		}

		try {
			setError('');
			registerView ? await register(email, password) : login(email, password);
			navigate('/');
		} catch {
			registerView ? setError('Failed to create an account!') : setError('Failed to log in!');
		}
	}

	return (
		<>
			{ error && <span>{ error }</span> }
			<form onSubmit={ handleAction } className="basic-form-layout">
				<div>
					<label>Email</label>
					<input
						required
						value={ email }
						type="email"
						onChange={ (e) => setEmail(e.target.value) }
					/>
				</div>
				<div>
					<label>Password</label>
					<input
						required
						value={ password }
						type="password"
						onChange={ (e) => setPassword(e.target.value) }
					 />
				</div>
				{ registerView &&
					<div>
						<label>Password Confirmation</label>
						<input
							required
							value={ passwordConfirmation }
							type="password"
							onChange={ (e) => setPasswordConfirmation(e.target.value) }
						/>
					</div>
				}
				<Button label={ label }/>
			</form>
		</>
	);
}

export default AuthForm;
