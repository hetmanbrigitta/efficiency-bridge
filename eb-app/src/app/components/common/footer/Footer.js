import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../../../context/AuthContext';

const Footer = () => {
	const navigate = useNavigate();
	const { logout } = useAuth();
	const [error, setError] = useState('');

	const handleLogout = async() => {
		setError('');

		try {
			await logout();
			navigate('/login');
		} catch {
			setError('Failed to log out!');
		}
	}

	return (
		<>
			{ error && <span>{ error }</span> }
			<footer>
				<button onClick={ handleLogout }>Log Out</button>
			</footer>
		</>
	);
}

export default Footer;
