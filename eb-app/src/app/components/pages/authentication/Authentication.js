import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import AuthForm from '../../common/form/AuthForm';
import logo from '../../../images/logo.png';
import './_authentication.scss';

const Authentication = ({ label }) => {
	const [loginView, setLoginView] = useState(true);

	const toggleAuth = (tab) => {
		if (tab.currentTarget.id === 'login' ) {
			setLoginView(true);
		} else {
			setLoginView(false);
		}
	}

	return (
		<div className="auth auth_container">
			<div className="logo_wrapper">
				<img src={ logo } alt="Logo" />
			</div>
			<div className="auth_wrapper">
				<div className="auth-btn_container">
					<Link to="/register" className="auth-btn_wrapper">
						<button id="register" onClick={ toggleAuth } className={ `main-btn auth-btn ${ !loginView && "active-btn" }` }>Register</button>
					</Link>
					<Link to="/login" className="auth-btn_wrapper">
						<button id="login" onClick={ toggleAuth } className={ `main-btn auth-btn ${ loginView && "active-btn" }` }>Login</button>
					</Link>
				</div>
				<AuthForm label={ label } registerView={ !loginView }/>
			</div>
		</div>
	);
}

export default Authentication;
