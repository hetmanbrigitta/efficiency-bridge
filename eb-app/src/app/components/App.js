import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import { AuthProvider } from '../../context/AuthContext';
import PrivateRoute from '../../routes/PrivateRoute';

import Authentication from './pages/authentication/Authentication';
import Dashboard from './pages/dashboard/Dashboard';

const App = () => {
	return (
		<AuthProvider>
			<BrowserRouter>
				<Routes>
					<Route
						path="/register"
						element={
							<Authentication
								label="Register"
							/>
						}
					/>
					<Route
						path="/login"
						element={
							<Authentication
								label="Log In"
							/>
						}
					/>
					<Route
						path="*"
						element={
							<PrivateRoute>
								<Dashboard/>
							</PrivateRoute>
						}
					/>
				</Routes>
			</BrowserRouter>
		</AuthProvider>
	);
}

export default App;
