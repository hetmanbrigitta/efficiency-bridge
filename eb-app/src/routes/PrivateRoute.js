import React from 'react';
import { Navigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

const PrivateRoute = ({ children }) => {
	const { currentUser } = useAuth();
	console.log('privateRoute', currentUser);
	return currentUser ? children : <Navigate to="/login" />;
}

export default PrivateRoute;
