import React, { useContext, useState, useEffect } from 'react';
import { auth } from '../firebase';

const AuthContext = React.createContext();

export const useAuth = () => {
	return useContext(AuthContext);
}

export const AuthProvider = ({ children }) => {
	const [currentUser, setCurrentUser] = useState();
	const [loading, setLoading] = useState(true);

	const register = (email, password) => {
		return auth.createUserWithEmailAndPassword(email, password);
	}

	const login = (email, password) => {
		return auth.signInWithEmailAndPassword(email, password);
	}

	const logout = () => {
		return auth.signOut();
	}

	useEffect(() => { //only runs when the component is mounted
		const unsubscribeEvent = auth.onAuthStateChanged(user => {
			setCurrentUser(user);
			setLoading(false);
		});

		return unsubscribeEvent;
	}, []);

	const value = {
		currentUser,
		register,
		login,
		logout
	}

	return (
		<AuthContext.Provider value={ value }>
			{ !loading && children }
		</AuthContext.Provider>
	);
}
